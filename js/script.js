let billeder = ["url('../img/flower.webp')", "url('../img/garden.jpg')", "url('../img/rose.webp')"];
let i = 0;
function submitComplete() {
    if(document.getElementById("name").value == "Margrete" || document.getElementById("name").value == "Søren"){
        document.getElementById("submitText").innerHTML = "Thank you for submitting!";
        document.getElementById("submitText").style.color = "green";
    } else {
        document.getElementById("submitText").innerHTML = "Wrong name";
        document.getElementById("submitText").style.color = "red";

    }
    
}

function getDate() {
    document.getElementById("datoText").innerHTML = Date();
}

function skiftBillede() {
    document.body.style.backgroundImage = billeder[i];
    i++;
    if (i > 2){
        i = 0;
    }

}